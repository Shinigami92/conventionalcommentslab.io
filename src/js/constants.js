export const STARS_SELECTOR = ".js-stars";
export const GITLAB_HOST = "https://gitlab.com";
export const GITLAB_GQL_API = `${GITLAB_HOST}/api/graphql`;
